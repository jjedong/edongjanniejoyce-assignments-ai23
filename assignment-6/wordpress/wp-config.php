<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'jannie' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '4Xg!nYnF;01)d}xCI7= ?kN?7)C]G)CYOrKV>4):!Bdc,p| &Kc m.{_jCd4/|&4' );
define( 'SECURE_AUTH_KEY',  '&}@?S&3/HnSB%]o`mX/st/JF`:|GfUjyG*L@7Qk1FBV_7!nXsvQ_x??+|55N0y>k' );
define( 'LOGGED_IN_KEY',    '8}iHVB_*|7+>V$r*/k9k{ Jx^XVu65y-m{p29tx;uATm,@4>j>-t4s#BgjnA[_0n' );
define( 'NONCE_KEY',        'hT%tw&@83DLZf? bD?#UDgZzWxRyY56l0`T&4{.-9xL4m8Nn+D&L&/E)S5[RCflK' );
define( 'AUTH_SALT',        '.fv/ICKx1Yt(kj|WhS,Th?xk?]}HYSttB%]ZHc`4}P@H4ajQaE>P:?L}?G(nz%Ut' );
define( 'SECURE_AUTH_SALT', 'BR<R<hGw);huRX*.*eXwd(N%kscSXjhSfvKjywf5(:qT?FjD9xZILTwh[E^x5@(B' );
define( 'LOGGED_IN_SALT',   'LtQ25rK/?lQ(G*>*zK9+%y5g+m+kK$L&#sb1UpMSs&/?%NHwuI) `.yh}%J?O2VW' );
define( 'NONCE_SALT',       'X(HQDh,{V&]9)QZ8pc:_oYU[v7iJV.tZRoELx3z$In8I+|#+k&|xQuzkwLj!_2s{' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
